package demo;

import entity.*;

import java.util.List;

public class Demo {


    public static void main(String[] args) {
        Train train1 = getTrain();

        System.out.println(train1);

        System.out.println("Before sort by comfort level"+ System.lineSeparator()+"fdkslfds");
        train1.printPassengerCars();

        System.out.println("Sortoed by comfort level ");
        train1.sortPassengerCarsByComfortLevel();
        train1.printPassengerCars();

        //Search passengers car by Passengesr capacity
        List<PassengerCar> waggonsByPassengersCapacity = train1.getWaggonsByPassengersCapacity(5);
        System.out.println("passengers car found by Passengesr capacity");
        for (PassengerCar ps : waggonsByPassengersCapacity) {
            System.out.println(ps);
        }

    }

    private static Train getTrain() {
        PassengerCar ps1 = new CommonPassengerCar(100, 300, ComfortLevel.ECONOM, "abc");
        PassengerCar ps2 = new CoupePassengerCar(40, 80, ComfortLevel.REGULAR, "abc");
        PassengerCar ps3 = new VipPassengerCar(20, 40, ComfortLevel.PREMIUM, "abc");

        PassengerCar ps4 = new CommonPassengerCar(100, 300, ComfortLevel.ECONOM, "abc");
        PassengerCar ps5 = new CoupePassengerCar(40, 80, ComfortLevel.REGULAR, "abc");
        PassengerCar ps6 = new VipPassengerCar(20, 40, ComfortLevel.PREMIUM, "abc");

        PassengerCar ps7 = new CommonPassengerCar(75, 300, ComfortLevel.ECONOM, "abc");
        PassengerCar ps8 = new CoupePassengerCar(25, 80, ComfortLevel.REGULAR, "abc");
        PassengerCar ps9 = new VipPassengerCar(15, 40, ComfortLevel.PREMIUM, "abc");

        PassengerCar ps10 = new CommonPassengerCar(85, 300, ComfortLevel.ECONOM, "abc");
        PassengerCar ps11 = new CoupePassengerCar(35, 80, ComfortLevel.REGULAR, "abc");
        PassengerCar ps12 = new VipPassengerCar(5, 40, ComfortLevel.PREMIUM, "abc");


        Train train1 = new Train("Mariupol-Kiev");
        train1.addPassengerCar(ps1);
        train1.addPassengerCar(ps2);
        train1.addPassengerCar(ps3);
        train1.addPassengerCar(ps4);
        train1.addPassengerCar(ps5);
        train1.addPassengerCar(ps6);
        train1.addPassengerCar(ps7);
        train1.addPassengerCar(ps8);
        train1.addPassengerCar(ps9);
        train1.addPassengerCar(ps10);
        train1.addPassengerCar(ps11);
        train1.addPassengerCar(ps12);

        return train1;
    }
}
