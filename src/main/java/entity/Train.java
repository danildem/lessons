package entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Train {

    private String destination;
    private List<PassengerCar> passengerCars = new ArrayList<>();


    public Train(String destination) {
        this.destination = destination;
    }

    public void addPassengerCar(final PassengerCar ps){
        this.passengerCars.add(ps);
    }

    public void removePassengerCar(final PassengerCar ps){
        this.passengerCars.remove(ps);
    }

    public int PassengerCarLenght(){
        return  passengerCars.size();
    }

    public int getPassangerCapacity() {
        int capacity=0;
        for (PassengerCar ps : passengerCars) {
            capacity += ps.getPassangerCapacity();
        }
        return capacity;
    }

    public int getBaggageCapacity() {
        int capacity=0;
        for (PassengerCar ps : passengerCars) {
            capacity += ps.getBaggageCapacity();
        }
        return capacity;
    }

    //sort
    public void sortPassengerCarsByComfortLevel(){
        Collections.sort(this.passengerCars, new PassengerCarByComfortComparator());
    }


    public void  printPassengerCars(){
        for (PassengerCar ps : this.passengerCars) {
            System.out.println(ps);
        }
    }

    public List<PassengerCar> getWaggonsByPassengersCapacity(int capacity) {
        List<PassengerCar> result = new ArrayList<>();
        for (PassengerCar ps : passengerCars) {
            if (ps.getPassangerCapacity() == capacity) {
                result.add(ps);
            }
        }
        return result;
    }

    @Override
    public String toString() {
        return "Train Informations {" +
                "destination=: " + destination + '\'' +
                ", Passenger carcs capacity: " + passengerCars.size() +
                ", Passengers capacity: " + this.getPassangerCapacity() +
                ", Baggage capacity: " + this.getBaggageCapacity() +
                '}';
    }
}