package entity;

import java.util.Comparator;

class PassengerCarByComfortComparator implements Comparator<PassengerCar> {

    public int compare(PassengerCar a, PassengerCar b){

        return a.getComfortLevel().compareTo(b.getComfortLevel());
    }
}
