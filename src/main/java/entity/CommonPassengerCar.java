package entity;

public class CommonPassengerCar extends PassengerCar {

    private String description;

    public CommonPassengerCar(int passangerCapacity, int baggageCapacity, ComfortLevel comfortLevel, String description ) {
        super(passangerCapacity, baggageCapacity, comfortLevel);
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
