package entity;

public class CoupePassengerCar extends PassengerCar {

    private String description;

    public CoupePassengerCar(int passangerCapacity, int baggageCapacity, ComfortLevel comfortLevel, String description ) {
        super(passangerCapacity, baggageCapacity, comfortLevel);
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
