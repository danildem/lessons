package entity;

public class PassengerCar {
    private int passangerCapacity;
    private int baggageCapacity;
    private ComfortLevel comfortLevel;


    public PassengerCar(int passangerCapacity, int baggageCapacity, ComfortLevel comfortLevel) {
        this.passangerCapacity = passangerCapacity;
        this.baggageCapacity = baggageCapacity;
        this.comfortLevel = comfortLevel;
    }

    public int getPassangerCapacity() {
        return passangerCapacity;
    }

    public void setPassangerCapacity(int passangerCapacity) {
        this.passangerCapacity = passangerCapacity;
    }

    public int getBaggageCapacity() {
        return baggageCapacity;
    }

    public void setBaggageCapacity(int baggageCapacity) {
        this.baggageCapacity = baggageCapacity;
    }

    public ComfortLevel getComfortLevel() {
        return comfortLevel;
    }

    public void setComfortLevel(ComfortLevel comfortLevel) {
        this.comfortLevel = comfortLevel;
    }



    @Override
    public String toString() {
        return "PassengerCar{" +
                "passangerCapacity=" + passangerCapacity +
                ", baggageCapacity=" + baggageCapacity +
                ", comfortLevel=" + comfortLevel +
                '}';
    }
}
